from django.contrib import admin
from .models import *

admin.site.register(Dokter)
admin.site.register(Pasien)
admin.site.register(Obat)
admin.site.register(Resep)
admin.site.register(JumlahObat)
admin.site.register(Token)