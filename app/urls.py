from django.urls import re_path, path
from .views import *

#Website urls
urlpatterns = [
    path('', homepageView, name='home'),
    path('login/', loginView, name='login'),
    path('logout/', logoutView, name='logout'),
    path('register/', registerView, name='register'),
    path('resep/', resepView, name='resep'),
    path('detail-resep/<int:id>', detailResepView, name='detailResep'),
    path('buat-resep/', buatResepView, name='buatResep'),
    path('tebus-resep/<int:id>', tebusResepView, name='tebusResep'),
    path('api/get-token', apiGetTokenFromMachine, name='getToken'),
    path('api/success', apiSuccess, name='success'),
]
