from apotek.settings import SECRET_KEY
from datetime import timedelta, datetime
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from django.http import Http404, JsonResponse
from django.utils.timezone import get_current_timezone
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from binascii import hexlify
from random import SystemRandom
from .models import *
from .forms import *

DOKTER = 0
PASIEN = 1

def authMachine(request):
    cipher = AES.new(SECRET_KEY[:16].encode(), AES.MODE_CBC, iv=request.GET['iv'].encode())
    enc_SECRET_KEY = hexlify(cipher.encrypt(pad(SECRET_KEY.encode(), 16))).decode()
    return request.GET['SECRET_KEY'] == enc_SECRET_KEY

def getExpiryTime():
    expiry_time = datetime.now(tz=get_current_timezone())+ timedelta(minutes=5)
    return expiry_time

def makeToken(resep):
    num = [1, 2, 3, 4, 5, 6]
    while True:
        try:

            gen = SystemRandom()
            token = ''
            for i in range(6):
                token += str(num[gen.randrange(10**5, 10**6) % 10])
            token = Token(resep=resep, token=str(token), expiry=getExpiryTime())
            token.save()
            break
        except:
            pass

    return token

def cekRole(user):
    dokter = Dokter.objects.filter(user=user)
    pasien = Pasien.objects.filter(user=user)
    if (len(dokter) != 0):
        return DOKTER
    return PASIEN

def homepageView(request):
    if (request.user.is_authenticated):
        return redirect('resep')
    else:
        return redirect('login')

def registerView(request):
    if (request.user.is_authenticated):
        return redirect('resep')

    response = {}
    if (request.method == 'POST'):
        form = RegisterForm(request.POST)

        valid = True
        try:
            role = int(request.POST['peran'])
            assert role == DOKTER or role == PASIEN
        except:
            valid = False

        if (form.is_valid() and valid):
            user = form.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)

            if (role == DOKTER):
                dokter = Dokter(user=user)
                dokter.save()
            else:
                pasien = Pasien(user=user)
                pasien.save()
            
            request.session['role'] = role
            return redirect('resep')
        else:
            response['error'] = 'Terdapat kesalahan, silakan coba lagi'
    
    response['DOKTER'] = DOKTER
    response['PASIEN'] = PASIEN

    return render(request, 'register.html', response)

def loginView(request):
    if (request.user.is_authenticated):
        return redirect('resep')
    
    response = {}
    if (request.method == 'POST'):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            role = cekRole(user)
            request.session['role'] = role
            return redirect('resep')
        else:
            response['error'] = 'Username / Password salah'

    return render(request, 'login.html', response)

def logoutView(request):
    logout(request)
    return redirect('login')

def resepView(request):
    if (not request.user.is_authenticated):
        return redirect('login')

    response = {}
    role = request.session['role']
    if (role == DOKTER):
        resep = Resep.objects.filter(dokter=request.user.dokter)
    else:
        resep = Resep.objects.filter(pasien=request.user.pasien)
    response['resep'] = resep

    return render(request, 'resep.html', response)

def detailResepView(request, id):
    if (not request.user.is_authenticated):
        return redirect('login')

    response = {}

    resep = Resep.objects.get(id=id)
    if (resep.pasien.user != request.user and request.session['role'] != DOKTER):
        raise Http404
    response['resep'] = resep

    jumlahObat = JumlahObat.objects.filter(resep=resep)
    response['jumlahObat'] = jumlahObat

    return render(request, 'detailResep.html', response)

def buatResepView(request):
    if (not request.user.is_authenticated):
        return redirect('login')

    role = request.session['role']
    if (role != DOKTER):
        raise Http404

    response = {}
    if (request.method == 'POST'):
        obat = Obat.objects.all()
        more_than_one_exists = False
        for o in obat:
            field = 'obat-%d' % o.id
            if (int(request.POST[field]) > 0):
                more_than_one_exists = True
                break
        if (more_than_one_exists):
            pasien = Pasien.objects.get(id=int(request.POST['pasien']))
            resep = Resep(dokter=request.user.dokter, pasien=pasien, status=Resep.BELUM_DIAMBIL)
            resep.save()
            for o in obat:
                field = 'obat-%d' % o.id
                if (int(request.POST[field]) > 0):
                    jumlahObat = JumlahObat(resep=resep, obat=o, jumlah=int(request.POST[field]), jumlahDispensed=0)
                    jumlahObat.save()
            return redirect('resep')
    
    obat = Obat.objects.all()
    pasien = Pasien.objects.all()
    
    response['obat'] = obat
    response['pasien'] = pasien

    return render(request, 'buatResep.html', response)

def tebusResepView(request, id):
    # membuat token atas sebuah resep
    # jika sudah ada untuk resep yang sama, hapus yang sebelumnya
    # expiry 5 menit

    if (not request.user.is_authenticated):
        return redirect('login')

    response = {}

    resep = Resep.objects.get(id=id)
    if ((resep.pasien.user != request.user and 
        request.session['role'] != DOKTER) or
        (resep.status == Resep.SUDAH_DIAMBIL)):
        return redirect('detailResep', id)

    response['resep'] = resep

    jumlahObat = JumlahObat.objects.filter(resep=resep)
    response['jumlahObat'] = jumlahObat

    try:
        token = resep.token
        if (token.isExpired()):
            token.delete()
            token = makeToken(resep)
    except:
        token = makeToken(resep)

    response['token'] = token

    return render(request, 'tebusResep.html', response)

def apiGetTokenFromMachine(request):
    # mesin mengirimkan request berisi token yang di-input ke mesin
    # pastikan secret key mereka sama (request harus dari mesin)

    # cek apakah token tersebut ada di db dan belum expired

    # jika tidak, kirimkan error

    # membuat token
    response = {}
    if (request.method == 'GET'):
        try:
            if (authMachine(request)):
                token = request.GET['token']
                token = Token.objects.filter(token=token)
                if (len(token) != 0 and not(token[0].isExpired())):
                    jumlahObat = JumlahObat.objects.filter(resep=token[0].resep)
                    listObat = []
                    for j in jumlahObat:
                        if (j.jumlah - j.jumlahDispensed > 0):
                            dictObat = {}
                            dictObat['nama'] = j.obat.nama_obat
                            dictObat['jumlah'] = j.jumlah - j.jumlahDispensed
                            listObat.append(dictObat)
                    response['resep'] = listObat
                    return JsonResponse(response)
        except Exception as e:
            print(e)
            
    response['error'] = 'error'
    return JsonResponse(response)

def apiSuccess(request):
    # mesin mengirimkan request berisi token yang sudah selesai diproses di mesin
    # cek apakah token ada di db
    # ubah status resep dari token
    # hapus token
    response = {}
    if (request.method == 'GET'):
        try:
            if (authMachine(request)):
                token = request.GET['token']
                token = Token.objects.filter(token=token)
                if (len(token) != 0 and not(token[0].isExpired())):
                    resep = token[0].resep
                    
                    jumlahObat = JumlahObat.objects.filter(resep=token[0].resep)

                    allDispensed = True
                    for j in jumlahObat:
                        try:
                            j.jumlahDispensed += int(request.GET[j.obat.nama_obat])
                        except:
                            pass
                        if (j.jumlahDispensed < j.jumlah):
                            allDispensed = False
                        j.save()
                    
                    if (allDispensed):
                        resep.status = Resep.SUDAH_DIAMBIL
                        resep.save()
                        token.delete()
                    
                    response['success'] = 'success'
                    return JsonResponse(response)
        except Exception as e:
            print(e)
            
    response['error'] = 'error'
    return JsonResponse(response)
