from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import get_current_timezone
from datetime import datetime

class Dokter(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    def __str__(self):
        return self.user.username

class Pasien(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    def __str__(self):
        return self.user.username

class Obat(models.Model):
    nama_obat = models.CharField(max_length=200)
    def __str__(self):
        return self.nama_obat

class Resep(models.Model):
    dokter = models.ForeignKey(Dokter, on_delete=models.CASCADE)
    pasien = models.ForeignKey(Pasien, on_delete=models.CASCADE)

    BELUM_DIAMBIL = 0
    SUDAH_DIAMBIL = 1
    STATUS_CHOICES = (
        (BELUM_DIAMBIL, 'belum diambil'),
        (SUDAH_DIAMBIL, 'sudah diambil'),
    )
    status = models.IntegerField(choices=STATUS_CHOICES)

    def __str__(self):
        return 'id: %d - dokter: %s - pasien: %s - status: %s' % (self.id, str(self.dokter), str(self.pasien), self.STATUS_CHOICES[self.status][1])

class JumlahObat(models.Model):
    resep = models.ForeignKey(Resep, on_delete=models.CASCADE)
    obat = models.ForeignKey(Obat, on_delete=models.CASCADE)
    jumlah = models.PositiveIntegerField()
    jumlahDispensed = models.PositiveIntegerField()

    def __str__(self):
        return '%s - obat: %s - jumlah: %d' % (str(self.resep), str(self.obat), self.jumlah)

class Token(models.Model):
    resep = models.OneToOneField(Resep, on_delete=models.CASCADE)
    token = models.CharField(max_length=200, unique=True)
    expiry = models.DateTimeField(editable=False)

    def isExpired(self):
        return datetime.now(tz=get_current_timezone()) > self.expiry